﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HtmlAgilityPack;
using WebApi.Models;

namespace WebApi.Services
{
     public interface IWeatherService
    {
        WeatherServiceResult GetSearchPageData(string city);
        WeatherServiceResult GetHomePageData();
        HtmlDocument LoadDocument(String url);
    }
}
