﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Enums;
using WebApi.Models;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class WeatherController : Controller
    {
        private readonly IWeatherService _weatherService;
        public WeatherController(IWeatherService weatherService)
        {
            _weatherService = weatherService;
        }


        [HttpGet]
        [Produces("application/json", Type = typeof(WeatherServiceHomePageResult))]
        public async Task<IActionResult> Get()
        {
            var wdata = await Task.Run(() => _weatherService.GetHomePageData());
            if (wdata.Status == WeatherServiceResultCodes.Ok)
                return Ok(wdata);

            return new StatusCodeResult((int)HttpStatusCode.ServiceUnavailable);
        }

        [HttpGet("{city}")]
        [Produces("application/json", Type = typeof(WeatherServiceSearchResult))]
        public async Task<IActionResult> Get(string city)
        {
            if (String.IsNullOrEmpty(city))
                return BadRequest("city is required");

            var wdata = await Task.Run(() => _weatherService.GetSearchPageData(city));
            if (wdata.Status == WeatherServiceResultCodes.Ok)
                return Ok(wdata);

            return new StatusCodeResult((int)HttpStatusCode.ServiceUnavailable);
        }
    }
}
