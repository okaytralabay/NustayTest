﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HtmlAgilityPack;
using WebApi.Enums;
using WebApi.Models;

namespace WebApi.Services
{
    public class YrNoWeatherService : IWeatherService
    {
        private readonly string url_search = "http://www.yr.no/soek/soek.aspx?sted=";
        private readonly string ServiceUrl = "http://www.yr.no";

        int startDateIndex = 1;
        int endDateIndex = 4;


        public WeatherServiceResult GetSearchPageData(string city)
        {
            var result = new WeatherServiceSearchResult() { Service = ServiceUrl, SearchTerm = city };

            try
            {
                var document = LoadDocument($"{url_search}{Uri.EscapeDataString(city)}");
                var table = document.DocumentNode.SelectNodes("//table[contains(@class,'yr-table-search-results')]");
                var href = table?.First().Descendants("tr")
                        .Skip(1)
                        .Take(1)
                        .FirstOrDefault()?
                        .Descendants("a")
                        .FirstOrDefault()?
                        .GetAttributeValue("href", String.Empty);
                if (String.IsNullOrEmpty(href))
                    return result;

                var subdoc = LoadDocument($"{ServiceUrl}{href}");
                var subTables = subdoc.DocumentNode.SelectNodes("//table[contains(@class,'yr-table-overview2')]");
                if (subTables == null)
                    return result;

                foreach (var subTable in subTables)
                {
                    var datePart = subTable.SelectSingleNode("//caption").InnerText.Trim();
                    var subData =
                        subTable.Descendants("tr").Skip(1).Select(tr => tr.ChildNodes.Where(a => a.OriginalName == "td")
                            .Select(cell => cell.GetAttributeValue("title", cell.InnerText).Trim())
                            ).Select(x =>
                            {
                                var list = x.ToList();
                                return new City.Forecast()
                                {
                                    Date = $"{datePart} {list.First()}",
                                    Data = String.Join(" ", list.Skip(1))
                                };
                            });
                    result.Forecasts.AddRange(subData);

                }
                result.Status = WeatherServiceResultCodes.Ok;
            }
            catch (Exception ex)
            {
                //   
            }


            return result;
        }

        public WeatherServiceResult GetHomePageData()
        {
            var result = new WeatherServiceHomePageResult() { Service = ServiceUrl };

            try
            {
                #region parsing

                var document = LoadDocument(ServiceUrl);
                var table = document.DocumentNode.SelectNodes("//table[contains(@class,'yr-table')]").FirstOrDefault();
                if (table == null)
                    return result;

                var datesElementsPerCity = endDateIndex - startDateIndex;
                var data = table.SelectNodes(".//tr")
                        .Select(tr => tr.SelectNodes("th|td").ToList().Select(cell =>
                            cell.GetAttributeValue("title",
                                  cell.OriginalName == "th" ? cell.InnerText.Trim()
                                : String.Join(Environment.NewLine, cell.ChildNodes.Where(a => a.OriginalName == "a")
                                                .Select(a => $"{a.InnerText} | {ServiceUrl}{a.GetAttributeValue("href", "no link")}"))
                                )).ToList()).ToList();

                var header = data.First();

                result.CitiesList = data.Skip(1).Select(x => new City()
                {
                    Name = x.First(),
                    Links = x.Skip(datesElementsPerCity + 1).First().Split('\n').Select(l =>
                      {
                          var parts = l.Split('|');
                          return new Link() { Href = parts[1].Trim(), Text = parts[0].Trim() };
                      }).ToList(),
                    Forecasts = x.Skip(startDateIndex).Take(datesElementsPerCity)
                                         .Select((forecast, index) => new City.Forecast()
                                         {
                                             Data = forecast,
                                             Date = header[index + startDateIndex]
                                         }).ToList()
                }).ToList();
                #endregion

                result.Status = WeatherServiceResultCodes.Ok;
            }
            catch (Exception ex)
            {
                //log it
            }

            return result;
        }

        public virtual HtmlDocument LoadDocument(String url)
        {
            var web = new HtmlWeb();
            return web.Load(url);
        }

    }
}
