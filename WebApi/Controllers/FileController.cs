﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class FileController : Controller
    {
        [HttpPost]
        public IActionResult Post()
        {
            if (!Request.Form.Files.Any() || Request.Form.Files.Count > 1)
            {
                return BadRequest("only 1 file");
            }
            var rfile = Request.Form.Files.First();
            if (rfile.ContentType != "text/xml")
                return BadRequest("not xml");

            XDocument doc;
            using (var file = rfile.OpenReadStream())
            {
                try
                {
                    doc = XDocument.Load(file);
                }
                catch (XmlException ex)
                {
                    return BadRequest("cant load doc");
                }
            }
            //TODO validate

            var ns = doc.Root?.GetDefaultNamespace();
            var el = doc.Descendants(ns + "DateRange").FirstOrDefault();
            var start = el?.Attribute("InDate")?.Value;
            var end = el?.Attribute("OutDate")?.Value;
            return Json(new { start, end });
        }
    }
}
