﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WebApi.Enums;

namespace WebApi.Models
{
     public class City
    {
        public class Forecast
        {
            public String Date { get; set; }
            public String Data { get; set; }
        }

        public String Name { get; set; }
        public List<Forecast> Forecasts { get; set; }
        public List<Link> Links { get; set; }
    }

    public class Link
    {
        public String Href { get; set; }
        public String Text { get; set; }
    }

    public abstract class WeatherServiceResult
    {
        [JsonIgnore]
        public WeatherServiceResultCodes Status { get; set; } = WeatherServiceResultCodes.NotOk;

        [JsonProperty(Order = 0)]
        public String LastParsingUtcTime => DateTime.UtcNow.ToString("hh:mm:ss", CultureInfo.InvariantCulture);

        [JsonProperty(Order = 3)]
        public String Service { get; set; }

        [JsonProperty(Order = 4)]
        public virtual int ResultsCount { get; protected set; }
    }

    public class WeatherServiceHomePageResult : WeatherServiceResult
    {
        [JsonProperty(Order = 10)]
        public List<City> CitiesList { get; set; }
        public override int ResultsCount => CitiesList?.Count ?? 0;
    }
    public class WeatherServiceSearchResult : WeatherServiceResult
    {
        [JsonProperty(Order = 5)]
        public String SearchTerm { get; set; }
        [JsonProperty(Order = 10)]
        public List<City.Forecast> Forecasts { get; set; } = new List<City.Forecast>();
        public override int ResultsCount => Forecasts?.Count ?? 0;
    }
}
