﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Mvc;
using Moq;
using WebApi.Controllers;
using WebApi.Models;
using WebApi.Services;
using Xunit;


namespace UnitTestProject
{
    public class UnitTest
    {
        public String dir { get; set; }
        public HtmlDocument empty_doc = new HtmlDocument();

        public UnitTest()
        {
            dir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
        }

        [Fact]
        public async Task Get_Returns_OkResult()
        {
            var path = Path.Combine(dir, "yrno_home.txt");
            var doc = new HtmlDocument();
            doc.LoadHtml(System.IO.File.ReadAllText(path));
            var service = new Mock<YrNoWeatherService>();
            service.Setup(s => s.LoadDocument(It.IsAny<String>())).Returns(doc);
            service.CallBase = true;

            var ctr = new WeatherController(service.Object);
            var result = await ctr.Get();
            dynamic jsonCollection = result;

            Assert.IsType<OkObjectResult>(result);
            Assert.IsAssignableFrom<WeatherServiceResult>(jsonCollection.Value);

            var wsResult = (WeatherServiceResult)jsonCollection.Value;
            Assert.Equal(8, wsResult.ResultsCount);
        }

        [Fact]
        public async Task Get_Returns_503_When_NoHtml()
        {
            var service = new Mock<YrNoWeatherService>();
            service.Setup(s => s.LoadDocument(It.IsAny<String>())).Returns(empty_doc);
            service.CallBase = true;

            var ctr = new WeatherController(service.Object);
            var result = await ctr.Get();

            Assert.IsType<StatusCodeResult>(result);
            Assert.IsAssignableFrom<StatusCodeResult>(result);

            var wsResult = (StatusCodeResult)result;
            Assert.Equal(503, wsResult.StatusCode);
        }

        [Fact]
        public async Task Get_Returns_BadRequest_When_Param_Is_Null_Or_Empty()
        {
            var service = new Mock<IWeatherService>();
            var ctr = new WeatherController(service.Object);

            var resultNull = await ctr.Get(null);
            Assert.IsType<BadRequestObjectResult>(resultNull);

            var resultEmpty = await ctr.Get("");
            Assert.IsType<BadRequestObjectResult>(resultEmpty);
        }

        [Fact]
        public async Task Get_Search_OkResult()
        {
            var path = Path.Combine(dir, "yrno_search_results.txt");
            var doc = new HtmlDocument();
            doc.LoadHtml(System.IO.File.ReadAllText(path));

            var path2 = Path.Combine(dir, "yrno_search_results_concrete.txt");
            var doc2 = new HtmlDocument();
            doc2.LoadHtml(System.IO.File.ReadAllText(path2));

            var service = new Mock<YrNoWeatherService>();
            service.SetupSequence(s => s.LoadDocument(It.IsAny<String>()))
                .Returns(doc).Returns(doc2);
            service.CallBase = true;

            var ctr = new WeatherController(service.Object);
            var result = await ctr.Get("Kiyv");
            dynamic jsonCollection = result;

            Assert.IsType<OkObjectResult>(result);
            Assert.IsAssignableFrom<WeatherServiceResult>(jsonCollection.Value);

            var wsResult = (WeatherServiceSearchResult)jsonCollection.Value;
            Assert.Equal("Kiyv", wsResult.SearchTerm);
            Assert.Equal(12, wsResult.ResultsCount);
        }
    }
}
